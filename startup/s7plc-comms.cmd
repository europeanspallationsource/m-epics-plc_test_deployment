########## Set up s7plc driver ##########
# The variable s7plcDebug can be set in the statup script or at any time on the command line to change the amount or debug output. The following levels are supported:
#
#-1:  fatal errors only
# 0:  errors only
# 1:  startup messages
# 2:+ output record processing
# 3:+ input record processing
# 4:+ driver calls
# 5:+ io printout
#
var s7plcDebug 0
#
# s7plc driver configuration:
#
#s7plcConfigure (PLCName, IPaddr, port, inSize, outSize, bigEndian, recvTimeout, sendInterval)
#
#where the arguments are:
#	PLCName - Arbitrary symbolic name for the PLC running a server TCP socket on IPaddr:port. The records reference the PLC with this name in their INP or OUT link. PLCName must not contain the slash character (/).
#	IPaddr - PLC IP address
#	port - port to use (default 2000)
#	inSize and outSize  - Data block sizes in bytes read from and sent to the PLC, respectively. Any of them can be 0.
#	bigEndian - defines byte order. If this is 1, the IOC expects the PLC to send and receive any multibyte PV (word, float, etc) most significant byte first. If it is 0, the data is least significant byte first. This is independent of the byte order of the IOC.
#	recvTimeout - If the IOC does not receive new data from the PLC for recvTimeout milliseconds, it closes the connection and tries to reopen it after a few seconds. recvTimeout should be 2 to 10 times the send intervall of the PLC.
#	sendInterval - The IOC checks for data to send every sendInterval milliseconds. If any output record has been processed in this time, the complete buffer is sent to the PLC. If no new output is available, nothing is sent.

# This module uses s7plc driver ONLY to read data from the PLC. In order to write data to the PLC, this module uses modbus driver (see next section). Therefore parameters outSize and sendInterval are set to 0.
#
s7plcConfigure($(PLCNAME), $(IPADDR), $(S7DRVPORT), $(INSIZE), 0, $(BIGENDIAN), $(RECVTIMEOUT), 0) 
####################
#
#
#
########## Set up modbus driver ##########
#Specify the TCP endpoint and give your bus a name eg. "PLC1".
#drvAsynIPPortConfigure(portName, hostInfo, priority, noAutoConnect, noProcessEos)
#where the arguments are:
#	portName - The portName that is registered with asynManager.
#	hostInfo - The Internet host name, port number and optional IP protocol of the device (e.g.
#				"164.54.9.90:4002", "serials8n3:4002", "serials8n3:4002 TCP" or "164.54.17.43:5186 udp"). If no
#				protocol is specified, TCP will be used. Possible protocols are:
#		TCP,
#		UDP,
#		UDP* — UDP broadcasts. The address portion of the argument must be the network broadcast address
#				(e.g. "192.168.1.255:1234 UDP*").
#		HTTP — Like TCP but for servers which close the connection after each transaction.
#		COM — For Ethernet/Serial adapters which use the TELNET RFC 2217 protocol. This allows port parameters
#				(speed, parity, etc.) to be set with subsequent asynSetOption commands just as for local serial
#				ports. The default parameters are 9600-8-N-1 with no flow control.
#	priority - Priority at which the asyn I/O thread will run. If this is zero or missing, then
#	epicsThreadPriorityMedium is used.
#	noAutoConnect - Zero or missing indicates that portThread should automatically connect. Non-zero if explicit
#					connect command must be issued.
#	noProcessEos - If 0 then asynInterposeEosConfig is called specifying both processEosIn and processEosOut.
drvAsynIPPortConfigure($(PLCNAME),$(IPADDR):$(MODBUSDRVPORT),0,0,1)
#
#Configure the interpose for TCP
#modbusInterposeConfig(portName, linkType, timeoutMsec, writeDelayMsec)
#where the arguments are:
#	portName - Name of the asynIPPort or asynSerialPort previously created.
#	linkType - Modbus link layer type:
#		0 = TCP/IP
#		1 = RTU
#		2 = ASCII
#	timeoutMsec - The timeout in milliseconds for write and read operations to the underlying asynOctet driver.
#					This value is used in place of the timeout parameter specified in EPICS device support. If
#					zero is specified then a default timeout of 2000 milliseconds is used.
#	writeDelayMsec - The delay in milliseconds before each write from EPICS to the device. This is typically only
#						needed for Serial RTU devices. The Modicon Modbus Protocol Reference Guide says this must
#						be at least 3.5 character times, e.g. about 3.5ms at 9600 baud, for Serial RTU. The
#						default is 0.
modbusInterposeConfig($(PLCNAME), 0, $(RECVTIMEOUT), 0)
#
#Creating Modbus ports
#drvModbusAsynConfigure(portName, tcpPortName, slaveAddress, modbusFunction,
#                       modbusStartAddress, modbusLength, dataType,
#                       pollMsec, plcType);
#where the arguments are:
#	portName - Name of the modbus port to be created.
#	tcpPortName - Name of the asyn IP or serial port previously created.
#	slaveAddress - The address of the Modbus slave. This must match the configuration of the Modbus slave (PLC) for
#					RTU and ASCII. For TCP the slave address is used for the "unit identifier", the last field in
#					the MBAP header. The "unit identifier" is ignored by most PLCs, but may be required by some.
#	modbusFunction - Modbus function code (1, 2, 3, 4, 5, 6, 15, 16, 123 (for 23 read-only), or 223 (for 23
#					write-only)). 
#	modbusStartAddress - Start address for the Modbus data segment to be accessed.
#						(0-65535 decimal, 0-0177777 octal).
#						For absolute addressing this must be set to -1.
#	modbusLength - The length of the Modbus data segment to be accessed.
#					This is specified in bits for Modbus functions 1, 2, 5 and 15.
#					It is specified in 16-bit words for Modbus functions 3, 4, 6, 16, or 23.
#					Length limit is 2000 for functions 1 and 2, 1968 for functions 5 and 15,
#					125 for functions 3 and 4, and 123 for functions 6, 16, and 23.
#	dataType - This sets the default data type for this port. This is the data type used if the drvUser field of a
#				record is empty, or if it is MODBUS_DATA. The supported Modbus data types and correponding drvUser
#				fields are described in the table below.
#	pollMsec - Polling delay time in msec for the polling thread for read functions.
#				For write functions, a non-zero value means that the Modbus data should be read once when the
#				port driver is first created.
#	plcType - Type of PLC (e.g. Koyo, Modicon, etc.). 
#				This parameter is currently used to print information in asynReport. It is also used to treat Wago
#				devices specially if the plcType string contains the substring "Wago". See the note below.
#
# This module uses modbus driver ONLY to write data to the PLC. In order to read data from the PLC, this module uses s7plc driver (see previous section). Therefore parameters outSize and sendInterval are set to 0.
#It is important to note, however, that many PLCs will time out sockets after a few seconds of inactivity. This is not a problem with modbus drivers that use read function codes, because they are polling frequently. But modbus drivers that use write function codes may only do occasional I/O, and hence may time out if they are the only ones communicating through a drvAsynIPPort driver. Thus, it is usually necessary for modbus drivers with write function codes to use the same drvAsynIPPort driver (socket) as at least one modbus driver with a read function code to avoid timeouts. Therefore this module configures "modbusread" port even though there will be no records configured to use it. Basically this is required to maintain connection and reconnect automatically if PLC restarts or drops out.
#
# Read port (will not be used but is configured so that PLC does not close connection)
drvModbusAsynConfigure("$(PLCNAME)read",	$(PLCNAME), 0, 3, 0, 1, 0, 1000, "S7-1500")
# Write port
drvModbusAsynConfigure("$(PLCNAME)write",	$(PLCNAME), 0, 6, -1, 2, 0, 0, "S7-1500")
####################
#
#
#
