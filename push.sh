#!/bin/bash -xe

if [ "" == "$1" ]; then
  echo "Need minor version number as argument:"
  echo "$ $0 x"
  echo "Creates tag v2.0.x "
  exit 1
fi

time git pull
git commit --allow-empty -m "Trigger test $1"
git tag -am 2.0.$1 v2.0.$1
time git push origin master:master
time git push origin tag v2.0.$1
